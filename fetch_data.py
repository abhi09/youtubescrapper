import YoutubeAPI as YAPI
import YoutubeDataObject as YDO
from datetime import datetime, timedelta
import util
from youtube_transcript_api import YouTubeTranscriptApi
import logging

###################################### INPUT PARAMS ############################################
query = "moisturizer review" # Query
max_res = 500  # results with each API call
csv_file_name = 'youtube_api_data.csv'
max_limit = 1000  # Limit want to stop after some x results
YObjects = []
start_page_token = None
delta_diff_date = 60
should_continue = 0
should_continue_limit = 2
###################################### INPUT PARAMS ############################################

def get_data_without_error(obj, key_list):
    try:
        for item in key_list:
            if item in obj.keys():
                obj = obj[item]
            else:
                return "NA"
        return obj
    except Exception:
        return "NA"


def get_all_data_from_query(query_string,start_after=None,start_before=None):
    next_page_token = start_page_token
    limit = 0
    total_items = 0
    while True:
        limit += 1
        logging.info("Current Page Token to resume : " + str(next_page_token))
        print("Current Page Token to resume : ", next_page_token)
        try:
            response = YAPI.search(query_string, page_token=next_page_token, max_results=max_res, start_after = start_after, start_before = start_before)
        except Exception as e:
            print(e)
            logging.info(f"Total Processes Item : {total_items}")
            print(f"Total Processes Item : {total_items}")
            break
        all_video_objects = []
        # Process 1st page
        page_info = response["pageInfo"]

        #pdb.set_trace()
        items_len = len(response["items"])
        total_items += items_len
        if items_len == 0:
            should_continue += 1
        else:
            should_continue = 0

        for item in response["items"]:
            # Checks whether given snippet is video
            if "videoId" in item["id"].keys():
                y_obj = YDO.YoutubeDataObject()
                now = datetime.now()
                current_time = now.strftime("%Y-%m-%d %H-%M-%S")
                y_obj.set_page_id(next_page_token)
                y_obj.set_scraping_time(current_time)
                y_obj.set_page_url("https://www.youtube.com/watch?v=" + get_data_without_error(item, ["id", "videoId"]))
                y_obj.set_video_id(get_data_without_error(item, ["id", "videoId"]))
                y_obj.set_video_date(get_data_without_error(item, ["snippet", "publishedAt"]))
                y_obj.set_channel_id(get_data_without_error(item, ["snippet", "channelId"]))
                y_obj.set_video_title(get_data_without_error(item, ["snippet", "title"]))
                y_obj.set_channel_name(get_data_without_error(item, ["snippet", "channelTitle"]))

                # Channel Info
                channel_response = YAPI.channel_info(y_obj.get_channel_id())
                for channel_item in channel_response["items"]:
                    y_obj.set_channel_start_date(get_data_without_error(channel_item, ['snippet', 'publishedAt']))
                    y_obj.set_channel_location(get_data_without_error(channel_item, ['snippet', "country"]))
                    y_obj.set_channel_total_views(get_data_without_error(channel_item, ['statistics', "viewCount"]))
                    y_obj.set_channel_total_vid(get_data_without_error(channel_item, ['statistics', "videoCount"]))
                    y_obj.set_n_subscribers(get_data_without_error(channel_item, ['statistics', "subscriberCount"]))

                # Video Info
                video_response = YAPI.video_details(y_obj.get_video_id())

                for video_item in video_response['items']:
                    y_obj.set_n_views(get_data_without_error(video_item, ['statistics', 'viewCount']))
                    y_obj.set_n_likes(get_data_without_error(video_item, ['statistics', 'likeCount']))
                    y_obj.set_n_dislikes(get_data_without_error(video_item, ['statistics', 'dislikeCount']))
                    y_obj.set_n_comments(get_data_without_error(video_item, ['statistics', 'commentCount']))
                    util.write_data(y_obj.get_video_id(), 'desc',
                                    get_data_without_error(video_item, ['snippet', 'description']))

                # Write transcript
                try:
                    transcript = YouTubeTranscriptApi.get_transcript(y_obj.get_video_id())
                    if transcript:
                        transcript_string = ""
                        for item in transcript:
                            transcript_string += item.get('text', None) + " "
                        y_obj.set_transcript('T')
                        util.write_data(y_obj.get_video_id(), 'transcript', transcript_string)
                    else:
                        y_obj.set_transcript('F')
                except Exception as e:
                    y_obj.set_transcript('F')

                all_video_objects.append(y_obj)

        if items_len != 0:
            util.write_to_csv_from_object(all_video_objects, csv_file_name)

        next_page_token = response.get("nextPageToken",None)

        if not next_page_token:
            logging.info(f"Total Processes Item : {total_items}")
            print(f"Total Processes Item : {total_items}")
            break

        if limit == max_limit:
            logging.info(f"Total Processes Item : {total_items}")
            print(f"Total Processes Item : {total_items}")
            next_page_token = None
            break
    return next_page_token

def run():
    start_before = datetime.utcnow()
    start_before = start_before - timedelta(2 * delta_diff_date)
    next_page_token = None
    while True:
        start_after = (start_before - timedelta(delta_diff_date))
        print(f'Processing Dates : start_after - {start_after.isoformat("T")}, start_before - {start_before.isoformat("T")}')
        logging.info(f'Processing Dates : start_after - {start_after.isoformat("T")}, start_before - {start_before.isoformat("T")}')
        next_page = get_all_data_from_query(query,start_after.isoformat("T") + 'Z',start_before.isoformat("T") + 'Z')
        start_before = start_after
        if should_continue == should_continue_limit:
            break

run()
