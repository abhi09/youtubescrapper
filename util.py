from YoutubeDataObject import YoutubeDataObject
import csv
from os import path
import logging


directory = "Data"
logging.basicConfig(filename="stats.log", level=logging.INFO)

def write_to_csv(dictionary, filename):
    csv_columns = dictionary[0].keys()
    csv_file = directory + '/' + filename
    write_header = not path.exists(csv_file)
    try:
        with open(csv_file, 'a+') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            if write_header:
                writer.writeheader()
            for data in dictionary:
                writer.writerow(data)
    except IOError:
        print("I/O error")
    logging.info("Successfully written to the file...")
    print("Successfully written to the file...")


def write_to_csv_from_object(youtube_object_list, filename):
    dict_obj = []
    for obj in youtube_object_list:
        dict_obj.append(YoutubeDataObject.user_to_dict(obj))
    write_to_csv(dict_obj, filename)


def write_data(video_id, suffix_part, data):
    f = open(directory + '/' + video_id+'_'+suffix_part+'.txt', "w")
    f.write(data)
    f.close()


#TESTING
'''
a = YoutubeDataObject()
a.set_scraping_time("1234")
a.set_channel_id("jchdsbchjd")
a.set_closed_captions("bdhbjfbjbj")
a.set_transcript("xbdchdzb")
write_to_csv_from_object([a,a,a], "test.csv")
'''
