import os

import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors


scopes = ["https://www.googleapis.com/auth/youtube.force-ssl","https://www.googleapis.com/auth/youtube.readonly"]


def authenticate():
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"
    client_secrets_file = "YOUR_CLIENT_SECRET_FILE.json"

    # Get credentials and create an API client
    flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(
        client_secrets_file, scopes)
    credentials = flow.run_console()
    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, credentials=credentials)
    return youtube


youtube_auth = authenticate()


def search(query, part='snippet', page_token=None, max_results=50,start_after = None, start_before = None):
    if start_after and start_before:
        if page_token:
           request = youtube_auth.search().list(
               part=part,
               q=query,
               pageToken=page_token,
               maxResults=max_results,
               publishedBefore=start_before,
               publishedAfter=start_after
           )
        else:
            request = youtube_auth.search().list(
                part=part,
                q=query,
                publishedBefore=start_before,
                publishedAfter=start_after
             )
    else:
        if page_token:
           request = youtube_auth.search().list(
               part=part,
               q=query,
               pageToken=page_token,
               maxResults=max_results
           )
        else:
            request = youtube_auth.search().list(
                part=part,
                q=query
             )
    response = request.execute()
    return response


def channel_info(channel_id, part='snippet,statistics'):
    request = youtube_auth.channels().list(
        part=part,
        id=channel_id
    )

    response = request.execute()

    return response


def video_details(video_id, part='snippet,statistics'):
    request = youtube_auth.videos().list(
        part=part,
        id=video_id
    )
    response = request.execute()
    return response
