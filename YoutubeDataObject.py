class YoutubeDataObject(object):

    def __init__(self):
        self.scrapingTime = None
        self.pageURL = None
        self.pageID = None
        self.videoID = None
        self.videoTitle = None
        self.videoDate = None
        self.channelName = None
        self.channelID = None
        self.channelStartDate = None
        self.channelLocation = None
        self.channelTotalVid = None
        self.channelTotalViews = None
        self.nSubscribers = None
        self.nViews = None
        self.nComments = None
        self.nLikes = None
        self.nDislikes = None
        self.Transcript = None

    def user_to_dict(x):
        return vars(x)

    def set_scraping_time(self, val):
        self.scrapingTime = val

    def set_page_url(self, val):
        self.pageURL = val

    def set_page_id(self, val):
        self.pageID = val

    def set_video_id(self, val):
        self.videoID = val

    def set_video_title(self, val):
        self.videoTitle = val

    def set_video_date(self, val):
        self.videoDate = val

    def set_channel_name(self, val):
        self.channelName = val

    def set_channel_id(self, val):
        self.channelID = val

    def set_channel_start_date(self, val):
        self.channelStartDate = val

    def set_channel_location(self, val):
        self.channelLocation = val

    def set_channel_total_vid(self, val):
        self.channelTotalVid = val

    def set_channel_total_views(self, val):
        self.channelTotalViews = val

    def set_n_subscribers(self, val):
        self.nSubscribers = val

    def set_n_views(self, val):
        self.nViews = val

    def set_n_comments(self, val):
        self.nComments = val

    def set_n_likes(self, val):
        self.nLikes = val

    def set_n_dislikes(self, val):
        self.nDislikes = val

    def set_transcript(self, val):
        self.Transcript = val

    def get_scraping_time(self):
        return self.scrapingTime

    def get_page_url(self):
        return self.pageURL

    def get_page_id(self):
        return self.pageID

    def get_video_id(self):
        return self.videoID

    def get_video_title(self):
        return self.videoTitle

    def get_video_date(self):
        return self.videoDate

    def get_channel_name(self):
        return self.channelName

    def get_channel_id(self):
        return self.channelID

    def get_channel_start_date(self):
        return self.channelStartDate

    def get_channel_location(self):
        return self.channelLocation

    def get_channel_total_vid(self):
        return self.channelTotalVid

    def get_channel_total_views(self):
        return self.channelTotalViews

    def get_n_subscribers(self):
        return self.nSubscribers

    def get_n_views(self):
        return self.nViews

    def get_n_comments(self):
        return self.nComments

    def get_n_likes(self):
        return self.nLikes

    def get_n_dislikes(self):
        return self.nDislikes

    def get_transcript(self):
        return self.Transcript
